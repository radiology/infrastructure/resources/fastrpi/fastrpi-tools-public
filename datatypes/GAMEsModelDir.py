# Copyright 2011-2014 Biomedical Imaging Group Rotterdam, Departments of
# Medical Informatics and Radiology, Erasmus MC, Rotterdam, The Netherlands
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import fastr
from fastr.data import url
from fastr.core.version import Version
from fastr.datatypes import URLType


class GAMEsModelDir(URLType):
    description = 'GAMEs Model directory'
    extension = ''

    def _validate(self):
        value = self.value

        if url.isurl(self.value):
            value = url.get_path_from_url(value)

        try:
            if not os.path.isdir(value):
                return False

            contents = os.listdir(value)
            valid = True
            # Check all the minimal components of a GAMEs model
            # Including surface points, point distribution model
            # and meshes
            # TODO It might well be better to make this a function
            # in GAMEs itself
            valid &= 'GAMEs.ini' in contents
            valid &= 'Model' in contents
            valid &= 'Original' in contents
            valid &= 'PCA' in contents
            valid &= 'Pmap' in contents
            valid &= 'Selection.txt' in contents
            valid &= 'SVC' in contents
            if valid:
                contents = os.listdir(os.path.join(value, 'Original'))
                valid &= 'Controls-Avg.txt' in contents
                valid &= 'Controls-AvgVolumeInfo.txt' in contents
                valid &= 'Controls-Avg_Stripped.txt' in contents
            if valid:
                contents = os.listdir(os.path.join(value, 'Model'))
                valid &= 'Controls-Avg.msh' in contents
                valid &= 'TrainedModel.msh' in contents
            if valid:
                contents = os.listdir(os.path.join(value, 'Pmap'))
                valid &= 'Controls-Avg_InsideVecs.txt' in contents
                valid &= 'EigenVectors.txt' in contents
                valid &= 'EigenVectors_Angles.txt' in contents
                valid &= 'PDM_Projections.txt' in contents
            return valid
        except ValueError:
            return False

    def action(self, name):
        if name is None:
            pass
        elif name == "ensure":
            if url.isurl(self.value):
                dir_name = url.get_path_from_url(self.value)
            else:
                dir_name = self.value

            fastr.log.debug('ensuring {} exists.'.format(dir_name))
            if not os.path.exists(dir_name):
                os.makedirs(dir_name)
        else:
            fastr.log.warning("unknown action")
